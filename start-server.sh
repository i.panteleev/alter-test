#!/bin/sh

set -o errexit
set -o pipefail
set -o nounset

cd ./api/

export DJANGO_SETTINGS_MODULE=config.settings

python manage.py collectstatic --noinput
python manage.py migrate --noinput
python manage.py loaddata account

uvicorn config.asgi:application --workers 3 --host 0.0.0.0 --port $PORT