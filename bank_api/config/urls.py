from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.views.generic.base import TemplateView
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

schema_view = get_schema_view(
    openapi.Info(
        title="Bank API",
        default_version="v1",
        terms_of_service="",
        contact=openapi.Contact(email="igor.panteleev1@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    url=f"{'https' if settings.HAS_TLS else 'http'}://{settings.HOST}/",
    public=True,
    permission_classes=(permissions.AllowAny,),
)


urlpatterns = [
    path("bank/", include("bank.urls")),
    path(
        "", schema_view.with_ui("swagger", cache_timeout=0), name="schema-swagger-ui",
    ),
    # verify host for loader.io
    path(
        "loaderio-e5548a4f337d1184942516cefbcd8577/",
        TemplateView.as_view(template_name="loaderio-e5548a4f337d1184942516cefbcd8577.html"),
        name="loader"
    ),
]
