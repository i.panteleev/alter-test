from django.db import models


class Account(models.Model):
    number = models.CharField(primary_key=True, max_length=20)
    balance = models.DecimalField(max_digits=19, decimal_places=4)

    def withdraw(self, amount):
        Account.objects.filter(number=self.number).update(
            balance=models.F("balance") - amount
        )

    def deposit(self, amount):
        Account.objects.filter(number=self.number).update(
            balance=models.F("balance") + amount
        )

    class Meta:
        verbose_name = "Account"
        verbose_name_plural = "Accounts"


class Transaction(models.Model):
    datetime = models.DateTimeField(auto_now_add=True)
    from_account = models.ForeignKey(
        "bank.Account", null=True, on_delete=models.SET_NULL, related_name="outcoming"
    )
    to_account = models.ForeignKey(
        "bank.Account", null=True, on_delete=models.SET_NULL, related_name="incoming"
    )
    amount = models.DecimalField(max_digits=19, decimal_places=4)
