from rest_framework.routers import SimpleRouter

from .views import AccountViewSet, TransactionViewSet

simple_router = SimpleRouter()

simple_router.register("account", AccountViewSet, basename="account")
simple_router.register("transaction", TransactionViewSet, basename="transaction")

urlpatterns = simple_router.urls
