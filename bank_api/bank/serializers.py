from datetime import datetime

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import Account, Transaction


class AccountSerializer(serializers.ModelSerializer):
    def validate_number(self, value):
        if not value.isdigit():
            raise ValidationError("Account number contains non-numeric characters.")
        return value

    class Meta:
        model = Account
        fields = (
            "number",
            "balance",
        )
        extra_kwargs = {"number": {"min_length": 20}, "balance": {"min_value": 0}}


class TransactionResponseSerializer(serializers.ModelSerializer):
    from_account = AccountSerializer()
    to_account = AccountSerializer()

    class Meta:
        model = Transaction
        fields = (
            "datetime",
            "from_account",
            "to_account",
            "amount",
        )


class TransactionSerializer(serializers.ModelSerializer):
    def validate(self, attrs):
        amount = attrs.get("amount")
        from_account = attrs.get("from_account")
        to_account = attrs.get("to_account")

        if to_account == from_account:
            raise ValidationError(
                {"non_field_errors": ["You can't transfer money to yourself."],}
            )

        accounts = Account.objects.select_for_update().filter(
            number__in=[from_account.number, to_account.number]
        )
        for account in accounts:
            if account.number == from_account.number:
                attrs["from_account"] = from_account = account
            else:
                attrs["to_account"] = to_account = account

        if from_account.balance < amount:
            raise ValidationError(
                {
                    "non_field_errors": [
                        f'Sender "{from_account.number}" doesn\'t have enough balance.'
                    ],
                }
            )
        return attrs

    def create(self, validated_data):
        from_account = validated_data.get("from_account")
        to_account = validated_data.get("to_account")

        transaction = super().create(validated_data)

        from_account.withdraw(transaction.amount)
        to_account.deposit(transaction.amount)

        return transaction

    def to_internal_value(self, data):
        """
        Override to return actual Account info even on validation failure.
        """
        try:
            return super().to_internal_value(data)
        except ValidationError as err:
            err.detail["rollback_data"] = {
                account: AccountSerializer(
                    Account.objects.get(number=data[account])
                ).data
                for account in ["from_account", "to_account"]
                if account not in err.detail
            }
            raise err

    class Meta:
        model = Transaction
        fields = (
            "datetime",
            "from_account",
            "to_account",
            "amount",
        )
        extra_kwargs = {
            "amount": {"min_value": 0},
            "is_success": {"read_only": True},
            "from_account": {
                "required": True,
                "error_messages": {
                    "does_not_exist": 'Account "{pk_value}" does not exist.',
                },
            },
            "to_account": {
                "required": True,
                "error_messages": {
                    "does_not_exist": 'Account "{pk_value}" does not exist.',
                },
            },
        }
