# Generated by Django 3.0.8 on 2020-07-04 16:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('number', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('balance', models.DecimalField(decimal_places=4, max_digits=19)),
            ],
            options={
                'verbose_name': 'Account',
                'verbose_name_plural': 'Accounts',
            },
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('datetime', models.DateTimeField(auto_now_add=True)),
                ('amount', models.DecimalField(decimal_places=4, max_digits=19)),
                ('from_account', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='outcoming', to='bank.Account')),
                ('to_account', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='incoming', to='bank.Account')),
            ],
        ),
    ]
