from decimal import Decimal

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from .models import Account, Transaction


class AccountTests(APITestCase):
    def test_create_account(self):
        NUMBER = "99999999999999999999"
        BALANCE = "100"

        url = reverse("account-list")
        data = {"number": NUMBER, "balance": BALANCE}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Account.objects.count(), 1)
        self.assertEqual(Account.objects.get().number, NUMBER)
        self.assertEqual(Account.objects.get().balance, Decimal(BALANCE))

    def test_create_account_unique_number(self):
        NUMBER = "99999999999999999999"
        BALANCE = "100"

        url = reverse("account-list")
        data = {"number": NUMBER, "balance": BALANCE}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Account.objects.count(), 1)
        self.assertEqual(Account.objects.get().number, NUMBER)
        self.assertEqual(Account.objects.get().balance, Decimal(BALANCE))

        data = {"number": NUMBER, "balance": BALANCE}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 1)

    def test_create_account_invalid_data(self):
        url = reverse("account-list")
        data = {"number": "12345678901234567890"}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 0)

        data = {"balance": "100"}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 0)

    def test_create_account_invalid_number(self):
        url = reverse("account-list")
        data = {"number": "1234567890123456789", "balance": "100"}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 0)

        data = {"number": "123456789012345678901", "balance": "100"}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 0)

        data = {"number": "-2345678901234567890", "balance": "100"}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 0)

    def test_create_account_invalid_balance(self):
        url = reverse("account-list")
        data = {"number": "12345678901234567890", "balance": "string"}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 0)

        data = {"number": "12345678901234567890", "balance": "-100.0"}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 0)

        data = {"number": "12345678901234567890", "balance": "12345678901234567890"}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 0)

        data = {"number": "12345678901234567890", "balance": "12345678901234.56789"}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Account.objects.count(), 0)

    def test_get_account(self):
        NUMBER = "99999999999999999999"
        account = Account.objects.create(number=NUMBER, balance="100")

        url = reverse("account-detail", kwargs={"pk": NUMBER})

        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["number"], account.number)
        self.assertEqual(Decimal(response.data["balance"]), Decimal(account.balance))

    def test_get_account_not_found(self):
        NUMBER = "99999999999999999999"

        url = reverse("account-detail", kwargs={"pk": NUMBER})

        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class TransactionTests(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.FROM_NUMBER = "00000000000000000000"
        cls.FROM_ACCOUNT_BALANCE = Decimal("100")
        cls.TO_NUMBER = "00000000000000000001"
        cls.TO_ACCOUNT_BALANCE = Decimal("0")

        cls.from_account = Account.objects.create(
            number=cls.FROM_NUMBER, balance=cls.FROM_ACCOUNT_BALANCE
        )
        cls.to_account = Account.objects.create(
            number=cls.TO_NUMBER, balance=cls.TO_ACCOUNT_BALANCE
        )

    def test_create_transaction(self):
        TRANSACTION_AMOUNT = "1"

        url = reverse("transaction-list")
        data = {
            "from_account": self.FROM_NUMBER,
            "to_account": self.TO_NUMBER,
            "amount": TRANSACTION_AMOUNT,
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Transaction.objects.count(), 1)
        self.assertEqual(
            Transaction.objects.get().from_account.balance,
            Decimal(self.FROM_ACCOUNT_BALANCE) - Decimal(TRANSACTION_AMOUNT),
        )
        self.assertEqual(
            Transaction.objects.get().to_account.balance,
            Decimal(self.TO_ACCOUNT_BALANCE) + Decimal(TRANSACTION_AMOUNT),
        )

    def test_create_transaction_invalid_data(self):
        url = reverse("transaction-list")
        data = {
            "from_account": self.FROM_NUMBER,
            "to_account": self.TO_NUMBER,
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Transaction.objects.count(), 0)
        self.assertEqual(
            Account.objects.get(number=self.FROM_NUMBER).balance,
            self.FROM_ACCOUNT_BALANCE,
        )
        self.assertEqual(
            Account.objects.get(number=self.TO_NUMBER).balance, self.TO_ACCOUNT_BALANCE
        )

        data = {
            "from_account": self.FROM_NUMBER,
            "balance": "1",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Transaction.objects.count(), 0)
        self.assertEqual(
            Account.objects.get(number=self.FROM_NUMBER).balance,
            self.FROM_ACCOUNT_BALANCE,
        )
        self.assertEqual(
            Account.objects.get(number=self.TO_NUMBER).balance, self.TO_ACCOUNT_BALANCE
        )

        data = {
            "to_account": self.TO_NUMBER,
            "balance": "1",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Transaction.objects.count(), 0)
        self.assertEqual(
            Account.objects.get(number=self.FROM_NUMBER).balance,
            self.FROM_ACCOUNT_BALANCE,
        )
        self.assertEqual(
            Account.objects.get(number=self.TO_NUMBER).balance, self.TO_ACCOUNT_BALANCE
        )

    def test_create_transaction_negative_amount(self):
        url = reverse("transaction-list")
        data = {
            "from_account": self.FROM_NUMBER,
            "to_account": self.TO_NUMBER,
            "balance": "-1",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Transaction.objects.count(), 0)
        self.assertEqual(
            Account.objects.get(number=self.FROM_NUMBER).balance,
            self.FROM_ACCOUNT_BALANCE,
        )
        self.assertEqual(
            Account.objects.get(number=self.TO_NUMBER).balance, self.TO_ACCOUNT_BALANCE
        )

    def test_create_transaction_not_enough_balance(self):
        url = reverse("transaction-list")
        data = {
            "from_account": self.FROM_NUMBER,
            "to_account": self.TO_NUMBER,
            "balance": "101",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Transaction.objects.count(), 0)
        self.assertEqual(
            Account.objects.get(number=self.FROM_NUMBER).balance,
            self.FROM_ACCOUNT_BALANCE,
        )
        self.assertEqual(
            Account.objects.get(number=self.TO_NUMBER).balance, self.TO_ACCOUNT_BALANCE
        )

    def test_create_transaction_to_yourself(self):
        url = reverse("transaction-list")
        data = {
            "from_account": self.FROM_NUMBER,
            "to_account": self.TO_NUMBER,
            "balance": "101",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Transaction.objects.count(), 0)
        self.assertEqual(
            Account.objects.get(number=self.FROM_NUMBER).balance,
            self.FROM_ACCOUNT_BALANCE,
        )
        self.assertEqual(
            Account.objects.get(number=self.TO_NUMBER).balance, self.TO_ACCOUNT_BALANCE
        )

    def test_create_transaction_account_does_not_exist(self):
        url = reverse("transaction-list")
        data = {
            "from_account": self.FROM_NUMBER,
            "to_account": self.TO_NUMBER,
            "balance": "1",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Transaction.objects.count(), 0)
        self.assertEqual(
            Account.objects.get(number=self.FROM_NUMBER).balance,
            self.FROM_ACCOUNT_BALANCE,
        )
        self.assertEqual(
            Account.objects.get(number=self.TO_NUMBER).balance, self.TO_ACCOUNT_BALANCE
        )

        data = {
            "from_account": self.FROM_NUMBER,
            "to_account": self.TO_NUMBER,
            "balance": "1",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Transaction.objects.count(), 0)
        self.assertEqual(
            Account.objects.get(number=self.FROM_NUMBER).balance,
            self.FROM_ACCOUNT_BALANCE,
        )
        self.assertEqual(
            Account.objects.get(number=self.TO_NUMBER).balance, self.TO_ACCOUNT_BALANCE
        )

    def test_get_transaction(self):
        transaction = Transaction.objects.create(
            from_account=self.from_account, to_account=self.to_account, amount="1"
        )

        url = reverse("transaction-detail", kwargs={"pk": transaction.pk})

        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            Decimal(response.data["from_account"]["balance"]),
            Decimal(self.from_account.balance),
        )
        self.assertEqual(
            Decimal(response.data["to_account"]["balance"]),
            Decimal(self.to_account.balance),
        )

    def test_get_transaction_not_found(self):
        pk = "1"

        url = reverse("transaction-detail", kwargs={"pk": pk})

        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
