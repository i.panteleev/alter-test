from django.db.transaction import atomic
from rest_framework import mixins, status
from rest_framework.viewsets import GenericViewSet
from rest_framework.response import Response

from .models import Account, Transaction
from .serializers import (
    AccountSerializer,
    TransactionSerializer,
    TransactionResponseSerializer,
)


class AccountViewSet(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    GenericViewSet,
):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer


class TransactionViewSet(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    GenericViewSet,
):
    queryset = Transaction.objects.all()

    def get_serializer_class(self):
        if self.request.method == "GET":
            return TransactionResponseSerializer
        return TransactionSerializer

    @atomic
    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        transactions = serializer.save()

        response = TransactionResponseSerializer(transactions).data

        return Response(response, status=status.HTTP_201_CREATED)
