FROM python:3.8-alpine

ARG requirements=requirements.txt
RUN adduser -D bank

WORKDIR /home/bank/

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk update \
    && apk add --virtual build-deps gcc python3-dev musl-dev build-base \
    && apk add postgresql-dev \
    && pip install psycopg2 setuptools uvicorn

COPY ./requirements.txt .
# add additional requirements for different environment, 
# for default value this command just override already copied requirements.txt
COPY ./$requirements .
RUN pip install -r ./$requirements

RUN apk del build-deps 

COPY bank_api ./api/
COPY start-server.sh .

RUN mkdir ./api/static && chown -R bank:bank ./api/static

USER bank

CMD ["/bin/sh", "/home/bank/start-server.sh"]
